# PPT
collapsed:: true
	- ![image.png](../assets/image_1714026327770_0.png)
	- ![image.png](../assets/image_1714026363974_0.png)
	- ![image.png](../assets/image_1714026375145_0.png)
	- ![image.png](../assets/image_1714026385757_0.png)
	-
	-
	-
- # 数据手册
	- # SDRAM
	- ## 端口
	- ### 控制信号
	- CLK,CKE
		- 时钟与时钟使能信号，表示时钟信号与时钟是否开启，时钟使能信号高电平表示时钟有效
	- CS
		- 片选信号，低电平有效
	- WE
	- CAS
	- RAS
	- ### 地址信号
	- A[12:0]
		- 地址
	- BA[1:0]
		- 块地址
	- ### 数据信号
	- DQ
		- 数据总线
	- ## 指令
	- ### 指令格式
	- {CS，RAS，CAS，WE}
	- ### 命令禁止（NOP）
	- 1,x,x,x
	- COMMAND INHIBIT功能用于防止设备执行新的命令，无论CLK信号是否启用。当COMMAND INHIBIT功能被激活时，设备实际上处于被取消选择的状态。然而，已经在进行中的操作并不会受到影响。这意味着即使有新的命令到达，设备也不会处理它们，而会继续完成当前正在执行的操作。这种功能在需要暂停设备操作或确保当前操作不被中断的场合非常有用。通过控制COMMAND INHIBIT信号，可以实现对设备操作的精确控制和管理。
	- ### 不操作（NOP）
	- 0,1,1,1
	- NOP（No Operation）命令是一种空操作命令。其主要目的是确保在执行其他命令之前，SDRAM已经被正确地选中，并且处于准备接收命令的状态。通过发送NOP命令，可以防止SDRAM接收到意外的、不正确的命令，从而确保后续的操作能够正常执行。NOP命令被用于对已经选择的SDRAM执行无操作，这有助于在空闲或等待状态期间防止发出不需要的命令。在实际应用中，NOP命令是SDRAM控制序列中的一部分，用于在初始化过程以及在不同操作之间提供适当的时序间隔。
	- ### 加载模式寄存器
	- 0,0,0,0
	- 模式寄存器是通过输入A[n:0]（其中An是最高有效地址项）、BA0和BA1来加载的（参见“模式寄存器”部分）。LOAD MODE REGISTER命令只有在所有Bank都处于空闲状态时才能发出，并且直到满足MRD（模式寄存器延迟）的要求之前，不能发出后续的可执行命令。
	  
	  模式寄存器在SDRAM中扮演着重要的角色，它存储了控制SDRAM操作模式的参数。这些参数可以包括突发长度、CAS延迟、预充电延迟等。通过正确设置模式寄存器，可以优化SDRAM的性能以满足特定的应用需求。
	  
	  LOAD MODE REGISTER命令是用于加载这些参数到模式寄存器的。然而，为了确保数据的一致性和完整性，这个命令只能在所有Bank都没有进行任何操作（即处于空闲状态）时才能执行。一旦这个命令被发出，SDRAM会开始加载模式寄存器，并且在这个过程完成之前，不能发出其他可执行命令。MRD（模式寄存器延迟）是一个时间参数，它定义了从发出LOAD MODE REGISTER命令到SDRAM准备好接受下一个命令所需的最短时间。
	  
	  因此，在使用SDRAM时，需要仔细控制和管理模式寄存器的加载过程，以确保操作的正确性和效率。
		- 模式寄存器定义了具体的操作模式，包括突发长度（BL）、突发类型、CAS延迟（CL）、操作模式和写突发模式。模式寄存器通过LOAD MODE REGISTER命令进行编程，并保留存储的信息，直到再次编程或设备断电。
		  
		  模式寄存器位M[2:0]指定BL；M3指定突发类型；M[6:4]指定CL；M7和M8指定操作模式；M9指定写突发模式；M10–Mn应设置为零，以确保与未来修订版的兼容性。Mn + 1和Mn + 2应设置为零以选择模式寄存器。
		  
		  当所有Bank都空闲时，必须加载模式寄存器，并且控制器必须等待tMRD时间才能启动后续操作。违反这些要求中的任何一个都将导致未指定的操作。
	- ### ACTIVE
	- 0,0,1,1
	- ACTIVE命令用于激活特定Bank中的一行，以便进行后续的访问操作。BA0和BA1输入端上的值用于选择Bank，而提供的地址则用于选择行。在PRECHARGE命令对该Bank发出之前，这一行会保持激活状态以进行访问。在打开同一Bank中的不同行之前，必须发出PRECHARGE命令。
	  
	  ACTIVE命令是SDRAM操作中的关键步骤之一。当CPU或控制器需要读取或写入某个存储单元时，它首先会通过ACTIVE命令激活包含目标行的Bank。激活行后，后续的读/写命令就可以快速定位到该行中的特定列，从而提高了访问速度。
	  
	  需要注意的是，每个Bank中只能有一行处于激活状态。因此，在打开同一Bank中的另一行之前，必须先对之前激活的行执行PRECHARGE命令，以关闭该行并释放资源。这样，就可以确保SDRAM能够正确、高效地处理多个访问请求。
	  
	  总的来说，ACTIVE和PRECHARGE命令在SDRAM操作中协同工作，确保数据能够准确、快速地被读取或写入。
	- ### READ
	- 0,1,0,1
	- READ命令用于启动对活动行的突发读取访问。BA0和BA1输入端上的值用于选择Bank，而提供的地址则用于选择起始列位置。输入A10上的值确定是否使用自动预充电。如果选择自动预充电，则在READ突发结束时会对正在访问的行进行预充电；如果未选择自动预充电，则该行将保持打开状态，以便进行后续访问。
	  
	  读取的数据会在DQ上出现，具体取决于两个时钟周期前DQM输入的逻辑电平。如果某个DQM信号被注册为高电平，则相应的DQ将在两个时钟周期后呈现高阻态（High-Z）；如果DQM信号被注册为低电平，则DQ将提供有效数据。
	  
	  通过这种方式，READ命令与DQM输入协同工作，实现对读取数据的精确控制。DQM允许用户精确地屏蔽掉不需要的数据，从而确保只有需要的数据被读取和处理。这在处理大量数据或需要精确控制数据读取的场合非常有用。
	  
	  总的来说，READ命令在SDRAM操作中扮演着重要角色，它使得CPU或控制器能够高效地读取存储单元中的数据，并通过DQM实现对读取数据的精确控制。
		- 一般使用不对DQM（数据掩码）进行操作
	- ### WRITE
	- 0,1,0,0
	- WRITE命令用于启动对活动行的突发写入访问。BA0和BA1输入端上的值用于选择Bank，而提供的地址则用于选择起始列位置。输入A10上的值确定是否使用自动预充电。如果选择自动预充电，则在写入突发结束时会对正在访问的行进行预充电；如果未选择自动预充电，则该行将保持打开状态，以便进行后续访问。
	  
	  出现在DQ上的输入数据将被写入内存数组，这取决于与数据同时出现的DQM输入逻辑电平。如果某个DQM信号被注册为低电平，则相应的数据将被写入内存；如果DQM信号被注册为高电平，则相应的数据输入将被忽略，并且不会对该字节/列位置执行写入操作。
	  
	  通过这种方式，WRITE命令与DQM输入协同工作，实现对写入数据的精确控制。DQM允许用户精确地屏蔽掉不需要写入的数据，从而防止不必要的数据覆盖。这在处理大量数据或需要精确控制数据写入的场合非常有用。
	  
	  总的来说，WRITE命令在SDRAM操作中起着关键作用，它使得CPU或控制器能够高效地将数据写入存储单元，并通过DQM实现对写入数据的精确控制。
	- ### PRECHARGE（预充电）
	- 0,0,1,0
	- PRECHARGE命令用于关闭特定Bank或所有Bank中的已打开行。在发出PRECHARGE命令后的指定时间（tRP）之后，Bank将可用于随后的行访问。输入A10确定是要预充电一个Bank还是所有Bank，如果仅预充电一个Bank，则输入BA0和BA1用于选择该Bank。否则，BA0和BA1将被视为“不关心”。在Bank被预充电之后，它将处于空闲状态，并且在向该Bank发出任何READ或WRITE命令之前，必须先将其激活。
	  
	  PRECHARGE命令在SDRAM操作中起着至关重要的作用。由于SDRAM在每个Bank中只能有一行处于激活状态，因此在进行新的行访问之前，必须先关闭之前激活的行。PRECHARGE命令就是用来完成这一任务的。通过精确控制PRECHARGE命令的发出时机，可以确保SDRAM始终处于最佳的工作状态，从而提供高效的数据访问能力。
	  
	  此外，PRECHARGE命令还可以与自动预充电功能结合使用。在READ或WRITE命令中，如果设置了自动预充电标志，那么在突发访问结束后，会自动对访问的行进行预充电。这简化了操作流程，提高了效率。
	  
	  总之，PRECHARGE命令是SDRAM操作中的关键步骤之一，它确保了Bank的正确管理和数据访问的高效性。
	- ### 自动刷新
	- 0,0,0,1
	- AUTO REFRESH在SDRAM的正常操作中使用，类似于传统DRAM中的CAS#-BEFORE-RAS# (CBR)刷新。这个命令是非持久的，因此每次需要刷新时都必须发出。在发出AUTO REFRESH命令之前，必须对所有活动的Bank进行预充电。在发出PRECHARGE命令后，必须满足最小tRP时间要求，才能发出AUTO REFRESH命令，如Bank/Row Activation（第51页）所示。
	  
	  在AUTO REFRESH命令期间，地址是由内部刷新控制器生成的。这使得地址位在AUTO REFRESH命令期间成为“不关心”的。无论设备宽度如何，256Mb的SDRAM每64ms（商业和工业用途）或16ms（汽车用途）需要8192个AUTO REFRESH周期。每7.813μs（商业和工业用途）或1.953μs（汽车用途）发出一个分布式的AUTO REFRESH命令，可以满足刷新要求并确保每一行都被刷新。或者，也可以在最小周期率（tRFC）下以突发方式发出8192个AUTO REFRESH命令，每64ms（商业和工业用途）或16ms（汽车用途）一次。
	  
	  总之，AUTO REFRESH是SDRAM正常操作中不可或缺的一部分，用于确保存储器的数据保持最新状态。通过正确地管理和定时AUTO REFRESH命令，可以确保SDRAM的性能和稳定性。
	- ### 不常用
	- 突发中断
		- 0,1,1,0
		- BURST TERMINATE命令用于截断固定长度或连续页面突发访问。在BURST TERMINATE命令之前最近注册的READ或WRITE命令将被截断。
		  
		  这个命令在SDRAM操作中提供了一种灵活性，允许控制器在突发访问过程中提前结束操作。当控制器需要中断当前的突发访问以处理其他任务或响应其他请求时，可以使用BURST TERMINATE命令来立即停止当前的READ或WRITE操作。
		  
		  通过截断突发访问，控制器可以避免不必要的数据传输，减少资源浪费，并提高系统的响应速度。这在需要快速切换任务或处理中断的情况下尤为重要。
		  
		  需要注意的是，使用BURST TERMINATE命令时，应确保在正确的时机发出该命令，以避免对正在进行的数据访问造成不利影响。此外，控制器应正确处理截断操作后可能存在的数据一致性和完整性问题。
		  
		  总之，BURST TERMINATE命令提供了一种在SDRAM突发访问过程中提前结束操作的机制，增加了系统的灵活性和响应速度。
	- 自主刷新
		- 0,0,0,1
		- SELF REFRESH命令可用于在系统其余部分断电时保留SDRAM中的数据。在自刷新模式下，SDRAM可以在没有外部时钟的情况下保留数据。
		  
		  SELF REFRESH命令的启动方式与AUTO REFRESH命令类似，但CKE必须禁用（低电平）。SELF REFRESH命令被注册后，除了CKE（必须保持低电平）之外，SDRAM的所有输入都变为“不关心”。
		  
		  进入自刷新模式后，SDRAM提供自己的内部时钟，使其能够执行自己的AUTO REFRESH周期。SDRAM必须保持在自刷新模式下至少tRAS指定的最短时间，并可以在此之后保持无限期的自刷新模式。
		  
		  退出自刷新模式需要一系列命令。首先，在CKE回到高电平之前，CLK必须稳定（稳定的时钟定义为在时钟引脚指定的时序约束内循环的信号）。CKE为高电平后，由于需要完成任何正在进行的内部刷新，SDRAM必须发出NOP命令（至少两个时钟周期）以满足tXSR的要求。
		  
		  退出自刷新模式后，必须按指定的间隔发出AUTO REFRESH命令，因为SELF REFRESH和AUTO REFRESH都使用行刷新计数器。
		  
		  需要注意的是，汽车温度设备不支持自刷新功能。
	- ## 操作
	- ### 初始化
	- SDRAM必须按照预定的方式加电并初始化。如果采用的操作程序不符合规定，可能会导致未定义的操作。在VDD和VDDQ（同时）上电且时钟稳定（稳定时钟定义为在时钟引脚规定的时序约束内循环的信号）之后，在发出除COMMAND INHIBIT或NOP之外的任何命令之前，SDRAM需要100微秒的延迟。在这个100微秒期间的某个时间点开始，并且至少持续到该时期结束，必须应用COMMAND INHIBIT或NOP命令。
	  
	  简而言之，为了确保SDRAM的正确和可靠操作，需要在上电并稳定时钟后等待一定的时间（100微秒），并且在这段时间内必须发送COMMAND INHIBIT或NOP命令。这是初始化过程中的一个重要步骤，可以防止因操作不当导致的未定义行为或错误。在此期间之后，可以安全地发出其他类型的命令以进一步配置和使用SDRAM。
	- 在至少应用了一个COMMAND INHIBIT或NOP命令并满足100微秒的延迟之后，应该应用一个PRECHARGE命令。然后，所有bank都必须进行预充电，从而使设备进入所有bank的空闲状态。
	  
	  一旦进入空闲状态，必须至少执行两个AUTO REFRESH周期。在完成AUTO REFRESH周期后，SDRAM就准备好进行模式寄存器编程了。由于模式寄存器在上电后会处于一个未知状态，因此在应用任何操作命令之前必须加载它。如果需要，可以在LMR命令之后发出两个AUTO REFRESH命令。
	  
	  简而言之，在SDRAM初始化过程中，需要首先等待并应用COMMAND INHIBIT或NOP命令，然后对所有bank进行预充电，使设备进入空闲状态。接着执行至少两次自动刷新操作，以确保数据完整性。最后，由于模式寄存器在上电后处于未知状态，必须在应用任何操作命令之前对其进行编程，以配置SDRAM的工作模式和参数。如果需要，可以在加载模式寄存器之后执行额外的自动刷新操作。
	- SDRAM推荐的加电顺序如下：
	  
	  1.同时对VDD和VDDQ上电。
	  
	  2.将CKE断言并保持在LVTTL逻辑低电平状态，因为所有输入和输出都是与LVTTL兼容的。
	  
	  3.提供稳定的CLOCK信号。稳定的时钟定义为在时钟引脚规定的时序约束内循环的信号。
	  
	  4.在发出除COMMAND INHIBIT或NOP之外的任何命令之前，至少等待100微秒。
	  
	  5.在这100微秒期间的某个时间点，将CKE拉高。至少在这段时间结束时，必须应用一个或多个COMMAND INHIBIT或NOP命令。
	  
	  6.执行一个PRECHARGE ALL命令。
	  
	  7.等待至少tRP时间；在此期间必须给出NOP或DESELECT命令。所有bank将完成预充电，从而使设备进入所有bank的空闲状态。
	  
	  8.发出一个AUTO REFRESH命令。
	  
	  9.等待至少tRFC时间，在此期间只允许NOP或COMMAND INHIBIT命令。
	  
	  10.再次发出一个AUTO REFRESH命令。
	  
	  11.再次等待至少tRFC时间，在此期间只允许NOP或COMMAND INHIBIT命令。
	  
	  12.现在SDRAM已准备好进行模式寄存器编程。由于模式寄存器在上电后会处于一个未知状态，因此在应用任何操作命令之前，应使用LMR命令将其加载为所需的位值。模式寄存器通过MODE REGISTER SET命令进行编程，其中BA1 = 0，BA0 = 0，并且保留存储的信息，直到再次编程或设备断电。如果初始化时不编程模式寄存器，将使用默认设置，这可能不是所期望的。在发出LMR命令后，输出保证为高阻态。在发出LMR命令之前，输出应该已经处于高阻态。
	  
	  13.等待至少tMRD时间，在此期间只允许NOP或DESELECT命令。
	  此时SDRAM已准备好。
		- 在序列中可以发出两个以上的AUTO REFRESH命令。在完成步骤9和10之后，可以重复这些步骤，直到达到所需的AUTO REFRESH + tRFC循环次数。
		  
		  简而言之，在完成初始的预充电和至少两次自动刷新操作后，如果需要，可以重复执行自动刷新和等待tRFC时间的步骤，以达到所需的自动刷新循环次数。这是为了确保SDRAM中的数据得到正确的刷新和保持，从而维持数据的完整性和可靠性。这个过程是SDRAM初始化过程中的重要环节，有助于确保设备在后续操作中能够正常工作。
	- ### 读写注意
	- 突发长度
		- 对设备的读取和写入访问是以突发为导向的，并且突发长度（BL）是可编程的。突发长度决定了给定READ或WRITE命令可以访问的最大列位置数。对于顺序和交错突发类型，都有1、2、4、8或连续位置的突发长度可选，而顺序类型还有连续页面突发可用。连续页面突发与BURST TERMINATE命令一起使用，以生成任意突发长度。
		  
		  不应使用保留状态，因为这可能导致未知操作或与未来版本的不兼容。
		  
		  当发出READ或WRITE命令时，将有效地选择一个与突发长度相等的列块。该突发的所有访问都在此块内进行，这意味着当达到边界时，突发将在块内循环。块是通过A[8:1]（当BL = 2时）、A[8:2]（当BL = 4时）和A[8:3]（当BL = 8时）唯一选择的。剩余的（最低有效位）地址位用于在块内选择起始位置。连续页面突发在到达页面边界时会在页面内循环。
	- 突发类型
		- 在给定的突发内，访问可以被编程为顺序或交错，这被称为突发类型，并通过位M3进行选择。
		  
		  突发内访问的顺序由突发长度、突发类型和起始列地址决定。
	- CAS延迟
		- CAS延迟（CL）是指从READ命令的注册到输出数据可用之间的时钟周期延迟。延迟可以设置为两个或三个时钟周期。
		  
		  如果READ命令在时钟边沿n处注册，且延迟为m个时钟周期，那么数据将在时钟边沿n + m处可用。DQ（数据队列）的驱动开始于时钟边沿前的一个周期（n + m - 1），只要满足相关的访问时间，数据在时钟边沿n + m处有效。例如，假设时钟周期时间足够满足所有相关的访问时间，如果READ命令在T0处注册，且延迟被编程为两个时钟周期，那么DQ将在T1之后开始驱动，数据将在T2处有效。
		  
		  不应使用保留状态，因为这可能导致未知操作或与未来版本的不兼容。
	- 操作模式
		- 正常操作模式是通过将M7和M8设置为零来选择的；M7和M8的其他值组合保留以供将来使用。不应使用保留状态，因为这可能导致未知操作或与未来版本的不兼容。
		  
		  简而言之，为了确保与当前和未来版本的兼容性以及避免任何未知的操作问题，用户应始终使用指定的值来设置M7和M8位，不要尝试使用那些为未来功能预留的状态。这样做可以确保内存设备的稳定性和可靠性，同时允许在未来可能的功能扩展中保持灵活性。
	- 写入突发配置
		- 当M9 = 0时，通过M[2:0]编程的突发长度适用于READ和WRITE突发；当M9 = 1时，编程的突发长度仅适用于READ突发，而写入访问是单一位置（非突发）访问。
		  
		  这意味着，通过配置M9位，可以灵活地控制内存设备的读写行为。当M9设置为0时，无论是读取还是写入操作，都会按照通过M[2:0]位设置的突发长度进行。这意味着连续的内存位置可以被一起访问，提高了数据吞吐率。
		  
		  然而，当M9设置为1时，写入操作变成单一位置的访问，即每次只写入一个位置，而不是按照突发长度进行。而读取操作仍然按照编程的突发长度进行。这种配置可能用于某些特定的应用场景，其中写入操作需要更精细的控制，而读取操作仍然可以受益于突发访问的性能提升。
		  
		  总之，通过M9和M[2:0]的配置，用户可以根据应用需求调整内存设备的读写行为，以实现最佳的性能和灵活性。
	- 行激活
		- 在向SDRAM中的某个bank发出任何READ或WRITE命令之前，必须先在该bank中打开一个行。这是通过ACTIVE命令完成的，该命令选择需要激活的bank和行。
		  
		  使用ACTIVE命令打开一行之后，可以在满足tRCD规范的前提下对该行发出READ或WRITE命令。tRCD（MIN）应该除以时钟周期并向上取整，以确定在ACTIVE命令之后最早可以输入READ或WRITE命令的时钟边沿。例如，如果tRCD规范为20ns，而时钟频率为125MHz（时钟周期为8ns），则结果为2.5个时钟周期，向上取整为3。这反映在图21（第51页）中，它涵盖了任何2 < tRCD（MIN）/tCK ≤ 3的情况。（相同的程序用于将其他规范限制从时间单位转换为时钟周期。）
		  
		  对同一bank中的不同行发出后续的ACTIVE命令，只能在之前的活动行被预充电之后进行。对同一bank连续发出ACTIVE命令之间的最小时间间隔由tRC定义。
		  
		  当第一个bank正在被访问时，可以发出另一个到不同bank的ACTIVE命令，这减少了总行访问的开销。对不同bank连续发出ACTIVE命令之间的最小时间间隔由tRRD定义。
	- 读操作
		- READ突发操作是通过READ命令发起的，如图15（第35页）所示。READ命令提供了起始列和bank地址，并为该突发访问启用或禁用自动预充电。如果启用了自动预充电，则在突发操作完成时会预充电正在访问的行。在后续的图示中，自动预充电是禁用的。
		  
		  在READ突发操作期间，从起始列地址开始的有效数据输出元素将在READ命令后的CAS延迟之后可用。每个后续的数据输出元素将在下一个正时钟边沿处有效。图23（第54页）展示了每种可能的CAS延迟设置的一般时序。
		  
		  突发操作完成后，假设没有发起其他命令，DQ信号将变为高阻态（High-Z）。连续页面突发操作将持续进行，直到被终止。在页面结束时，它会环绕到第0列并继续。
		  
		  任何READ突发操作的数据都可以通过后续的READ命令来截断，而固定长度的READ突发操作的数据可以立即跟随另一个READ命令的数据。在这两种情况下，都可以维持数据的连续流动。新突发操作的首个数据元素要么跟随已完成突发的最后一个元素，要么跟随被截断的更长突发的最后一个所需数据元素。新的READ命令应在最后一个所需数据元素有效的时钟边沿前x个周期发出，其中x = CL - 1。这在图23（第54页）中针对CL2和CL3进行了展示。
		  
		  SDRAM设备使用流水线架构，因此不需要与预取架构相关的2n规则。可以在任何READ命令之后的时钟周期发起READ命令。可以在同一bank中执行全速随机读取访问，或者每个后续的READ命令可以在不同的bank中执行。
			- 启动读操作：
			  读操作是通过READ命令启动的，该命令包含起始列地址和银行地址。
			  读操作可以配置为在突发访问完成后自动进行预充电（auto precharge），如果启用了自动预充电，那么被访问的行在突发完成后会被预充电。
			  读操作期间的数据输出：
			  在READ命令后，根据列访问延迟（CAS latency），从起始列地址开始的有效输出数据将会在READ命令之后的第n个时钟周期（n为CAS延迟值）可用。
			  随后的每个数据输出元素将在下一个正时钟边沿可用。
			  突发完成后的动作：
			  在一次突发传输完成后，如果没有其他命令被启动，数据总线（DQ）信号将变为高阻态（High-Z）。
			  连续页模式突发（continuous page burst）将持续进行，直到被终止。在页尾时，它将回绕到第0列并继续。
			  截断和连续的读操作：
			  任何读操作产生的数据都可以被后续的读操作截断。
			  固定长度的读操作后，可以立即跟随另一个读操作的数据。
			  在这两种情况下，都可以维持数据的连续流动。
			  新的读操作的第一个数据元素要么跟在完成的突发后面，要么跟在被截断的长突发的最后所需数据元素后面。
			  新的读命令应在最后所需数据元素有效的时钟边沿之前的x个周期发出，其中x = CL - 1。
			  SDRAM的流水线架构：
			  SDRAM使用流水线架构，并不需要预取架构中的2n规则。
			  读命令可以在任何时钟周期发出，跟在另一个读命令之后。
			  可以对同一银行执行全速随机读取，或者每个后续的读取可以在不同的银行执行。
	- ### 随机读
	- 任何READ突发操作的数据都可以通过后续的WRITE命令来截断，而固定长度的READ突发操作的数据可以紧接着WRITE命令的数据（受限于总线周转限制）。只要可以避免I/O竞争，WRITE突发操作可以在READ突发操作的最后一个（或最后一个所需）数据元素之后的时钟边沿立即发起。在特定的系统设计中，存在一种可能性，即驱动输入数据的设备会在DQ变为高阻态（High-Z）之前变为低阻态（Low-Z）。在这种情况下，最后一个读取的数据和WRITE命令之间应该至少有一个时钟周期的延迟。
	  
	  这意味着，当系统准备从READ突发操作切换到WRITE突发操作时，必须确保数据路径是稳定的，并且没有发生信号冲突。如果读取数据的驱动设备在DQ释放（变为高阻态）之前释放了其输出（变为低阻态），那么这可能会导致数据冲突或不确定的行为。为了避免这种情况，系统设计应该确保在发送WRITE命令之前，有足够的时间让读取数据的驱动设备稳定地释放其输出。
	  
	  通过引入这种延迟，系统可以确保READ和WRITE操作之间的顺利过渡，避免数据损坏或丢失，并维护系统的稳定性和可靠性。这是在进行高速数据传输和突发操作时，特别是在使用SDRAM等存储设备时，需要仔细考虑的重要方面。
	- DQM输入用于避免I/O竞争，如图24（第55页）和图25（第56页）所示。为了抑制READ的数据输出，DQM信号必须在WRITE命令之前的至少两个时钟周期内被断言（置为高电平）（DQM延迟对于输出缓冲区来说是两个时钟周期）。WRITE命令被注册后，无论DQM信号的状态如何，DQ都将变为高阻态（或保持高阻态），前提是DQM在截断READ命令的WRITE命令之前的时钟周期内处于激活状态。如果不是这样，那么第二次WRITE将是一个无效的WRITE。例如，如果DQM在T4期间为低电平，那么T5和T7的WRITE将是有效的，而T6的WRITE将是无效的。
	  
	  简言之，DQM输入是一个控制信号，用于在READ和WRITE操作之间切换时管理数据输出。通过提前断言DQM信号，系统可以确保在WRITE命令之前不会输出任何READ数据，从而避免I/O竞争和数据冲突。同时，系统必须确保DQM在正确的时间被激活，以便正确执行后续的WRITE操作。如果不这样做，就可能导致无效的WRITE操作，影响数据完整性和系统稳定性。
	- DQM信号必须在WRITE命令之前被取消断言（对于输入缓冲区，DQM延迟为零个时钟周期），以确保写入的数据不会被屏蔽。图24（第55页）展示了由于时钟周期频率的原因，在不需要添加NOP（无操作）周期的情况下如何避免总线竞争。而图25（第56页）则展示了需要额外添加NOP周期的情况。
	  
	  这意味着，在发送WRITE命令之前，必须确保DQM信号不再处于激活状态，以便写入的数据能够正常传输到目标位置。如果DQM信号在WRITE命令时仍处于激活状态，那么写入的数据可能会被屏蔽，导致数据丢失或错误。
	  
	  图24展示了在特定时钟频率下，可以通过精确控制DQM信号和WRITE命令的时序来避免总线竞争，而无需添加额外的NOP周期。这有助于提高数据传输的效率。
	  
	  相反，图25展示了在某些情况下，由于时序限制或其他因素，可能需要添加一个NOP周期来确保DQM信号和WRITE命令之间的正确同步。NOP周期是一个空闲周期，用于在数据传输过程中提供必要的延迟，以确保信号稳定和正确传输。
	  
	  因此，在设计和实现基于DQM信号的数据传输系统时，需要仔细考虑时钟频率、信号延迟和总线竞争等因素，以确保数据传输的准确性和可靠性。
	- 固定长度的READ突发操作可以通过对同一bank发出PRECHARGE命令来跟随或截断，前提是自动预充电功能未被激活。PRECHARGE命令应该在最后一个所需数据元素有效的时钟边沿之前的x个周期发出，其中x = CL - 1。这对于每个可能的CL都如图26（第56页）所示；数据元素n + 3是四个数据突发中的最后一个，或者是更长突发中的最后一个所需数据元素。在PRECHARGE命令之后，直到满足tRP（行预充电时间）之前，不能对同一bank发出后续命令。请注意，部分行预充电时间是在访问最后一个或多个数据元素期间隐藏的。
	  
	  对于执行到完成的固定长度突发，在最佳时间（如上所述）发出的PRECHARGE命令将提供与具有自动预充电功能的相同固定长度突发相同的操作。PRECHARGE命令的缺点是它要求命令和地址总线在适当的时间可用以发出命令。然而，PRECHARGE命令的优点在于它可以用于截断固定长度或连续的页面突发。
	  
	  简而言之，PRECHARGE命令可以用来控制对存储bank的访问，包括截断固定长度的READ突发操作或确保在完成突发操作后释放bank资源。通过精确控制PRECHARGE命令的发出时间，系统可以在满足时序要求的同时优化性能。然而，这也增加了对命令和地址总线可用性的依赖，因此在使用时需要仔细考虑。
	- ### 读>>?
	- 从任何READ突发传输的数据可以通过后续的WRITE命令来截断，而固定长度的READ突发传输后的数据可以立即跟随WRITE命令的数据（这取决于总线转换时间的限制）。只要可以避免I/O竞争，WRITE突发可以在READ突发最后一个（或期望的最后一个）数据元素之后的时钟边沿开始。在特定的系统设计中，存在一种可能性，即驱动输入数据的设备会在DQ变为高阻态之前进入低阻态。在这种情况下，最后一个读取的数据和WRITE命令之间应该至少有一个时钟周期的延迟。
	  
	  简而言之，SDRAM的数据读取和写入操作可以通过特定的命令进行灵活控制。在READ突发过程中，可以使用WRITE命令来截断数据的传输。然而，在实际应用中需要注意避免I/O竞争，以确保数据传输的准确性和稳定性。同时，考虑到系统设计中可能存在的数据驱动问题，需要在最后一个读取的数据和WRITE命令之间加入适当的延迟，以避免数据冲突和错误。这些操作和控制方式有助于提高SDRAM的数据传输效率和系统稳定性。
	- DQM输入用于避免I/O竞争，如图24（第55页）和图25（第56页）所示。DQM信号必须在WRITE命令之前至少两个时钟周期内被断言（置高），以抑制READ的数据输出（输出缓冲区的DQM延迟是两个时钟周期）。WRITE命令被注册后，无论DQM信号的状态如何，DQ都将变为高阻态（或保持高阻态），前提是在截断READ命令的WRITE命令之前的时钟周期内DQM是活动的。如果不是这样，那么第二次WRITE将是一个无效的WRITE。例如，如果DQM在T4期间为低电平，那么T5和T7时刻的WRITE将是有效的，而T6时刻的WRITE将是无效的。
	  
	  简而言之，DQM信号是一个关键的控制信号，用于在SDRAM操作中避免I/O竞争。通过正确地在WRITE命令之前断言DQM信号，可以确保在READ和WRITE操作之间不会发生数据冲突。如果DQM信号没有在正确的时间被断言，那么随后的WRITE操作可能会无效。因此，在设计和实现基于SDRAM的系统时，需要仔细控制DQM信号的时序，以确保数据传输的准确性和稳定性。
		- DQM延迟：DQM信号必须至少在写命令之前的两个时钟周期被置为高电平（HIGH），即断言。这是因为输出缓冲区的DQM延迟是两个时钟周期。这样做是为了确保在执行写命令之前，从读命令输出的数据被抑制。
		  写命令和高阻态（High-Z）：一旦写命令被寄存器记录，数据线（DQ）将变为高阻态（High-Z），即不驱动任何电平，无论DQM信号的状态如何。但这仅在DQM在写命令之前的时钟周期内是激活状态的情况下成立，这种情况下写命令会截断读命令。
		  无效的写操作：如果DQM在写操作之前的周期内不是激活状态（即，如果它是低电平LOW），则随后的写操作将是无效的。
		- 如果在T4周期DQM是低电平（LOW），
		  那么在T5和T7周期的写操作将是有效的，
		  而在T6周期的写操作将是无效的。
		  这是因为在T4周期DQM是LOW，所以在T5之前DQM必须变成HIGH，满足DQM至少在写命令前两个时钟周期被置为HIGH的要求。然后，只要在T5的写命令之前的周期（即T4）DQM是HIGH，T5的写命令就会有效。同理，T7的写命令也是有效的。但对于T6的写命令，由于在T5的时候DQM应该是HIGH（为了使T5的写命令有效），因此在T6周期DQM不会是在写命令之前两个周期内被置为HIGH，所以T6的写命令是无效的。
	- DQM信号必须在WRITE命令之前被取消断言（输入缓冲区的DQM延迟为零个时钟周期），以确保写入的数据不会被屏蔽。图24（第55页）展示了由于时钟周期频率，在没有添加NOP周期的情况下如何避免总线竞争的情况，而图25（第56页）则展示了需要额外NOP周期的情况。
	  
	  简而言之，为了确保写入的数据能够正确传输到SDRAM中，必须在WRITE命令之前取消对DQM信号的断言。根据时钟周期频率和系统设计的不同，有时可能需要添加NOP周期来避免总线竞争和数据冲突。因此，在实际应用中，需要根据具体情况灵活调整DQM信号和NOP周期的使用，以确保数据传输的准确性和稳定性。
	- 如果未激活自动预充电功能，固定长度的READ突发传输之后可以跟随或截断至同一bank的PRECHARGE命令。PRECHARGE命令应在最后一个期望的数据元素有效的时钟边沿之前的x个周期发出，其中x = CL - 1。这在图26（第56页）中针对每个可能的CL都有展示；数据元素n + 3要么是四个突发传输中的最后一个，要么是更长突发传输中期望的最后一个数据元素。在PRECHARGE命令之后，必须满足tRP时间才能对同一bank发出后续命令。请注意，部分行预充电时间是在访问最后一个或多个数据元素期间隐藏的。
	  
	  简而言之，在固定长度的READ突发传输后，如果未启用自动预充电功能，可以使用PRECHARGE命令来截断该突发或准备对同一bank进行其他操作。但在发出PRECHARGE命令后，需要等待一段时间（tRP）才能对同一bank发出新的命令。同时，部分预充电操作的时间会与最后一个或多个数据元素的访问时间重叠，从而提高了操作的效率。
	- 在固定长度突发传输执行完毕的情况下，在最佳时间（如上所述）发出PRECHARGE命令，其操作效果与具有自动预充电功能的相同固定长度突发传输相同。PRECHARGE命令的缺点是它要求命令和地址总线在合适的时间可用以发出命令。然而，PRECHARGE命令的优点在于它可以用于截断固定长度或连续页面突发传输。
	  
	  简而言之，虽然PRECHARGE命令需要额外的总线资源来在合适的时间发出，但它提供了截断突发传输的灵活性，这是自动预充电功能所不具备的。因此，在实际应用中，可以根据系统需求和资源情况选择是否使用PRECHARGE命令来优化SDRAM的操作。
	- 连续页面READ突发传输可以使用BURST TERMINATE命令来截断，而固定长度的READ突发传输在未激活自动预充电功能的情况下也可以使用BURST TERMINATE命令来截断。BURST TERMINATE命令应在最后一个期望的数据元素有效的时钟边沿之前的x个周期发出，其中x = CL - 1。这在图27（第57页）中针对每个可能的CAS延迟都有展示；数据元素n + 3是更长突发传输中期望的最后一个数据元素。
	  
	  简而言之，无论是连续页面还是固定长度的READ突发传输，只要没有启用自动预充电功能，都可以使用BURST TERMINATE命令来提前截断突发传输。这为用户提供了更大的灵活性，可以根据需要控制数据传输的长度，从而提高系统性能和效率。同时，用户需要确保在正确的时间发出BURST TERMINATE命令，以避免数据丢失或错误。
	- ### 写操作
	- WRITE突发传输是通过WRITE命令来启动的，如图16（第36页）所示。WRITE命令提供了起始列和bank地址，并且可以为该访问启用或禁用自动预充电功能。如果启用了自动预充电，那么在突发传输完成后，正在访问的行将被预充电。在以下图中使用的通用WRITE命令中，自动预充电功能是禁用的。
	  
	  简而言之，WRITE命令用于启动WRITE突发传输，并指定起始的列和bank地址。根据配置，可以选择是否启用自动预充电功能。如果启用，那么在突发传输结束后，相应的行会被自动预充电。但在一些通用的WRITE命令中，自动预充电功能是被禁用的。这为用户提供了灵活性，可以根据具体的应用场景和需求来配置和使用WRITE突发传输。
	- 在WRITE突发传输期间，第一个有效的数据输入元素与WRITE命令同时被寄存。随后的数据元素在每个连续的正时钟边沿上被寄存。在完成固定长度的突发传输后，假设没有启动其他命令，DQ将保持在高阻态（High-Z），并且任何额外的输入数据都将被忽略（参见图31，第61页）。连续页面突发传输将继续进行，直到被终止；在页面结束时，它将回到第0列并继续。
	  
	  简而言之，WRITE突发传输从WRITE命令发出时开始接收第一个有效数据，并在每个正时钟边沿上接收后续数据。固定长度的突发传输完成后，如果没有其他命令启动，数据总线将保持高阻态，不再接收新的输入数据。而连续页面突发传输则会在页面结束时回到起始列并继续，直到被终止。这些特性使得WRITE突发传输能够高效地处理大量数据的写入操作。
	- 任何WRITE突发传输的数据都可以通过后续的WRITE命令来截断，而固定长度的WRITE突发传输的数据可以紧接着另一个WRITE命令的数据。新的WRITE命令可以在前一个WRITE命令之后的任何时钟周期发出，并且与新命令同时提供的数据适用于新命令（参见图32，第62页）。数据n + 1要么是两个突发传输中的最后一个，要么是更长突发传输中期望的最后一个数据元素。
	  
	  简而言之，WRITE突发传输的数据可以通过后续的WRITE命令来截断，从而提前结束数据传输。同时，新的WRITE命令可以紧接着前一个命令发出，并且新命令的数据将应用于新的突发传输。这种灵活性使得用户可以更精确地控制数据写入的过程，以满足不同的应用需求。
	- SDRAM设备使用流水线架构，因此不需要与预取架构相关的2n规则。WRITE命令可以在前一个WRITE命令之后的任何时钟周期上启动。在同一bank内，可以执行全速随机写入访问，如图33（第63页）所示，或者每个后续的WRITE可以执行到不同的bank。
	  
	  简而言之，由于SDRAM采用流水线架构，因此没有预取架构中的2n规则限制。这意味着WRITE命令可以紧密地连续发出，不需要等待前一个命令完成。这种特性使得在同一bank内或不同bank之间都能实现高速的随机写入操作，从而提高了内存的性能和效率。
	- ### 写>>?
	- 任何WRITE突发传输的数据都可以通过后续的READ命令来截断，固定长度的WRITE突发传输的数据也可以紧接着一个READ命令。在READ命令被寄存后，数据输入将被忽略，并且不会执行WRITE操作（参见图34，第63页）。数据n + 1要么是突发传输中的最后一个数据（如果是两个数据的突发传输），要么是更长突发传输中期望的最后一个数据元素。
	  
	  简而言之，用户可以使用READ命令来截断WRITE突发传输，从而提前结束写操作并立即开始读操作。在READ命令被寄存后，系统将忽略后续的数据输入，并且不会执行任何WRITE操作。这种机制为用户提供了更大的灵活性和控制力，可以根据需要调整读写操作的顺序和长度。
	- 固定长度的WRITE突发传输的数据可以在不启用自动预充电的情况下，通过向同一bank发送PRECHARGE命令来跟随或截断。连续页面的WRITE突发传输可以通过向同一bank发送PRECHARGE命令来截断。PRECHARGE命令应在最后一个期望的输入数据元素被寄存后的tWR时钟周期内发出。无论频率如何，自动预充电模式都需要至少一个时钟周期的tWR时间来完成。
	  
	  简单来说，如果你不想等待固定长度的WRITE突发传输自然结束，你可以通过向相同的bank发送一个PRECHARGE命令来提前结束它。同样，对于连续页面的WRITE突发传输，你也可以使用PRECHARGE命令来截断它。但是，你需要注意在正确的时机发出PRECHARGE命令，确保最后一个期望的数据元素已经被寄存。此外，如果启用了自动预充电模式，那么无论工作频率如何，都需要至少一个时钟周期的tWR时间来完成预充电操作。
	- 此外，在高频时钟（tCK < 15ns）下截断WRITE突发传输时，必须使用DQM信号来屏蔽PRECHARGE命令之前和同时的时钟边沿上的输入数据（参见图35，第64页）。数据n + 1要么是突发传输中的最后一个数据（如果是两个数据的突发传输），要么是更长突发传输中期望的最后一个数据元素。在PRECHARGE命令之后，必须等待tRP时间才能向同一bank发出后续命令。
	  
	  简而言之，当在高频时钟下截断WRITE突发传输时，需要特别注意数据屏蔽的问题。通过使用DQM信号，可以确保在PRECHARGE命令执行前后不会接收到不需要的输入数据。同时，为了避免潜在的冲突和不稳定性，必须等待足够的时间（tRP）后才能对同一bank发出新的命令。这些措施有助于确保内存操作的正确性和稳定性。
	- 在固定长度突发传输执行完毕的情况下，如果在最佳时间（如上所述）发出PRECHARGE命令，将产生与自动预充电相同固定长度突发传输相同的操作效果。PRECHARGE命令的缺点是它要求命令和地址总线在适当的时间可用以发出命令。然而，PRECHARGE命令的优点在于它可以用来截断固定长度突发传输或连续页面突发传输。
	  
	  简而言之，PRECHARGE命令可以在固定长度突发传输完成后使用，其效果与自动预充电相同。但是，使用PRECHARGE命令需要确保命令和地址总线在正确的时间可用。尽管如此，PRECHARGE命令的灵活性使其成为截断固定长度突发传输或连续页面突发传输的有力工具。
	- 固定长度的WRITE突发传输可以通过BURST TERMINATE命令来截断。在截断WRITE突发传输时，与BURST TERMINATE命令同时应用的输入数据将被忽略。最后写入的数据（在DQM为低电平时）将是在BURST TERMINATE命令前一个时钟周期应用的输入数据。这如图36（第65页）所示，其中数据n是更长突发传输中期望的最后一个数据元素。
	  
	  简而言之，BURST TERMINATE命令可以用来提前结束固定长度的WRITE突发传输。当这个命令被触发时，与其同时的输入数据会被忽略，而最后一个被写入的数据是命令前一个时钟周期的数据（前提是在那个时钟周期内DQM信号为低电平）。这种机制为用户提供了更大的灵活性，可以根据需要随时截断突发传输。
	- ### 单写
	- 通过将模式寄存器中的写突发模式位（M9）编程为1，可以进入突发读取/单列写入模式。在这种模式下，无论编程的突发长度是多少，所有的WRITE命令都只会访问单个列位置（即突发长度为1）。而READ命令则根据编程的突发长度和序列来访问列，这与正常操作模式（M9=0）下相同。
	  
	  简单来说，为了启动连续写操作但每次只发送一个写指令，你需要将模式寄存器中的M9位设置为1。这样，每次发送WRITE命令时，它都只会写入一个列位置，而不是按照编程的突发长度连续写入多个位置。这种模式下，你可以通过连续发送多个WRITE命令来实现对多个不同列位置的写入，每次只写入一个位置。需要注意的是，在这种模式下，READ命令的行为与正常模式相同，会按照编程的突发长度和序列来读取列。