### 模块介绍
	- ![image.png](../assets/image_1712479634106_0.png)
	- DPC（Dead Pixel Correction）：坏点矫正
	  collapsed:: true
		- Dead Pixel Correction（死像素校正）是一种图像处理技术，用于修复或掩盖数码相机传感器、显示屏或其他数字图像设备上的死像素问题。所谓的“死像素”是指那些无法正常显示预期颜色的像素，它们可能始终显示为黑色（被称为“死亮”），始终显示为某一固定颜色（称为“卡住的像素”），或者根本不显示任何颜色（称为“死暗”）。
		- Dead Pixel Correction 通常通过软件算法来实现，该算法会检测出死像素，并利用周围正常像素的颜色信息来估算并替换掉这些损坏的像素点。在显示屏上，这可以帮助改善观看体验；而在数码相机的图像传感器上，这有助于提高最终图像的质量。
		- 在数码相机中，Dead Pixel Correction 可能是在摄像机内部完成的，即在将图像数据写入存储卡之前进行处理。而在显示屏或监视器中，这种校正通常是由屏幕的固件或驱动软件实施的。不过，值得注意的是，如果死像素数量较多，可能表明硬件存在较严重问题，这时候软件校正可能无法完全解决问题，可能需要更换硬件。
		- ![image.png](../assets/image_1712479766511_0.png)
		-
	- BLC（Black Level Correction）：黑色水平校正
	  collapsed:: true
		- 在无光照的情况下，黑色图像在数值上并不是0.需要减去一个平均值，让纯黑图像在数值上回归到0.
		- ![image.png](../assets/image_1712479931766_0.png)
		-
	- BNR（Bayer Noise Reducation）：在Bayer域上对图像进行降噪
	  collapsed:: true
		- ![image.png](../assets/image_1712480042717_0.png)
	- AF（Auto Focus）：自动对焦
	  collapsed:: true
		- 指相机以特定区域(一般指中央，但现在的系统已经可以指定在观景窗内看到的任何一点角落)，进行通过多种函数(灰度，梯度，拉普拉斯等）进行评估、进而调整镜头中镜片形成焦点，使照相机内的影像看起来清晰
		- ![image.png](../assets/image_1712480179284_0.png)
		-
	- LSC ( Lens Shading Correction ) :镜头阴影矫正
	  collapsed:: true
		- 镜头在捕捉图像时会因为物理结构的限制，在图像的边缘产生暗角或色彩失真
		- ![image.png](../assets/image_1712480317248_0.png)
		-
	- Demosaic :Bayer图转成RGB图
	  collapsed:: true
		- ![image.png](../assets/image_1712480396242_0.png)
		-
	- AWB：自动白平衡
	  collapsed:: true
		- ![image.png](../assets/image_1712480577898_0.png)
		-
	- CCM：颜色矫正
	  collapsed:: true
		- ![image.png](../assets/image_1712480494141_0.png)
	- CSC：色域转换
	  collapsed:: true
		- 将RGB域颜色转换成其他域颜色
		- ![image.png](../assets/image_1712480675544_0.png)
	- AE：自动曝光
	  collapsed:: true
		- ![image.png](../assets/image_1712480774753_0.png)
	- gamma：使亮度均匀变换
	  collapsed:: true
		- ![image.png](../assets/image_1712480833428_0.png)
	- 图像去噪与增强
	  collapsed:: true
		- ![image.png](../assets/image_1712480996815_0.png)
		-
-